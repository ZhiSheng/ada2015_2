#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct edge{
	int group;
	int u,v;
	int weight;
} EDGE;

typedef struct set{
	int parent;
	int size;
} SET;

int compare(const void *data1,const void *data2){
	EDGE *ptr1 = (EDGE *)data1;
	EDGE *ptr2 = (EDGE *)data2;
	if(ptr1->weight > ptr2->weight)
		return 1;
	else if(ptr1->weight < ptr2->weight)
		return -1;
	else
		return 0; 
}

int find(SET set[50001],int node){
	if(set[node].parent != node)
		set[node].parent = find(set,set[node].parent);
	return set[node].parent;
}

void UNION(SET set[50001],int x_id,int y_id){
	int min = (x_id < y_id)? x_id:y_id;
	if(x_id < y_id){
		set[y_id].parent = x_id;
		set[x_id].size += set[y_id].size;
		set[y_id].size = 0;
	}
	if(y_id < x_id){
		set[x_id].parent = y_id;
		set[y_id].size += set[x_id].size;
		set[x_id].size = 0;
	}
}

void find_MUST(EDGE edge[100000],int n,int m){
	SET set[50001];
	int dfs_edge[50001];
	int count = 0;
	int count1 = 0;
	int count2 = 0;
	int total_edge = 0;
	long long int total_weight = 0;
	long long int min_weight = 0;
	bool cycle = false;
	for(int i = 1;i <= n;i++){
		set[i].parent = i;
		set[i].size = 1;
	}

	for(int i = 0;i < m;i++){
		int x_id = find(set,edge[i].u);
		int y_id = find(set,edge[i].v);

		if(x_id != y_id){
			UNION(set,x_id,y_id);
			total_edge++;
			total_weight += edge[i].weight;
			min_weight += edge[i].weight;
			dfs_edge[count] = i;
			count++;
			continue;
		}
	}
	
	for(int i = 0;i < count;i++){
		 SET set2[50001];
		 long long int cal_weight = 0;
		 int cal_size = 0;
		 for(int j = 1;j <=n;j++){
			 set2[j].parent = j;
			 set2[j].size = 1;
		 }

		 for(int j = 0;j < m;j++){
			if(j == dfs_edge[i])
				continue;
			
			int x_id = find(set2,edge[j].u);
			int y_id = find(set2,edge[j].v);
			int min = (x_id < y_id)? x_id:y_id;
			if(x_id != y_id){
				cal_weight += edge[j].weight;
				UNION(set2,x_id,y_id);
				cal_size = set2[min].size;
				if(cal_weight > min_weight)
					break;
			}
		 }
		 if(cal_size == n && cal_weight == min_weight){
			total_edge--;
			total_weight -= edge[dfs_edge[i]].weight;
		 }
	}
	cout << total_edge << " " << total_weight << "\n";
}

int main(){
	int T,n,m,a,b,c;
	EDGE edge[100000];
	int set[50001];
	cin >> T;
	for(int i = 0;i < T;i++){
		cin >> n >> m;

		for(int j = 0;j < m;j++){
			cin >> a >> b >> c;
			edge[j].u = a;
			edge[j].v = b;
			edge[j].weight = c;
		}
		qsort(edge,m,sizeof(EDGE),compare);
		find_MUST(edge,n,m);
	}
}