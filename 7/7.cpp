#include <iostream>
#include <string.h>
#include <algorithm>
#include <functional>
using namespace std;

int degree1[1100];
int degree2[1100];
static int _count;

typedef struct Edge{
	int edge[1100];
	int selfloop;
	int multi_edge[1100];
	int multi_point_count;
	int multi_edge_count;
	int edge_connect;
	int point_connect[1100];
} EDGE;

typedef struct Degree{
	int vertex;
	int degree;
} DEGREE;

bool compare(EDGE edge1,EDGE edge2,EDGE edge_1[],EDGE edge_2[]){
	int tmp[11000] = {0};
	if(edge1.selfloop != edge2.selfloop || edge1.multi_edge_count != edge2.multi_edge_count || edge1.edge_connect != edge2.edge_connect || edge1.multi_point_count != edge2.multi_point_count)
			return false;
	for(int i = 0;i < edge1.edge_connect;i++){
		bool get = false;
		int tmp1 = edge1.point_connect[i];
		int _d1 = degree1[tmp1];
		for(int j = 0;j < edge2.edge_connect;j++){
			int tmp2 = edge2.point_connect[j];
			int _d2 = degree2[tmp2];
			if(tmp[j] != 0)
				continue;
			if(_d1 == _d2){
				if(edge_1[tmp1].selfloop != edge_2[tmp2].selfloop || edge_1[tmp1].multi_edge_count != edge_2[tmp2].multi_edge_count || edge_1[tmp1].edge_connect != edge_2[tmp2].edge_connect || edge_1[tmp1].multi_point_count != edge_2[tmp2].multi_point_count)
					continue;
				else{
					tmp[j] = 1;
					get =  true;
					break;
				}
			}
		}
		if(!get)
			return false;
	}
	return true;
}

bool waytosort(DEGREE const &i,DEGREE const &j){
	if(i.degree > j.degree)
		return true;
	else
		return false;
}

int take_answer(EDGE edge_1[],EDGE edge_2[],int used[],DEGREE degree_1[],DEGREE degree_2[],int cur,int save,int tmp[],int degree1[],int degree2[],int ret[]){
	int start = _count;
	int count = 0;
	for(int i = 0;i < edge_1[cur].edge_connect;i++){
		int tmp_1 = edge_1[cur].point_connect[i];
		if(used[tmp_1] != 0){
			continue;
		}
		for(int j = 0;j < edge_2[save].edge_connect;j++){
			int tmp_2 = edge_2[save].point_connect[j];
			if(tmp[tmp_2] != 0)
				continue;
			if(degree1[tmp_1]== degree2[tmp_2]){
				//try to break to make faster
				if(compare(edge_1[tmp_1],edge_2[tmp_2],edge_1,edge_2) && used[tmp_1] == 0){
					ret[_count] = tmp_1;
					used[tmp_1] = tmp_2;
					tmp[tmp_2] = 1;
					_count++;
					if(take_answer(edge_1,edge_2,used,degree_1,degree_2,tmp_1,tmp_2,tmp,degree1,degree2,ret))
						break;
					_count--;
					ret[_count] = 0;
					used[tmp_1] = 0;
					tmp[tmp_2] = 0;
				}
			}	
		}
		if(used[tmp_1] == 0){
			for(int j = start;j <= _count;j++)
				used[ret[j]] = 0;
			_count = start;
			return 0;
		}
	}
	return 1;
}

int fix_first(EDGE edge_1[],EDGE edge_2[],DEGREE degree_1[],DEGREE degree_2[],int used[],int n,int cur,int tmp[],int degree1[],int degree2[]){
	int count = 0;
	int save[1100] = {0};
	//for loop can be better if loop with same degree only
	for(int i = 1;i <= n;i++){
		if(degree_2[i].degree < degree_1[cur].degree){
			break;
		}
		else if(degree_2[i].degree > degree_1[cur].degree){
			continue;
		}
		if(degree_2[i].degree == degree_1[cur].degree){
			int point = degree_2[i].vertex;
			if(compare(edge_1[degree_1[cur].vertex],edge_2[point],edge_1,edge_2) && tmp[point] == 0){
				save[count] = point;
				count++;
			}
		}
	}
	for(int i = 0;i < count;i++){
		int ret[1100] = {0};
		_count = 0;
		used[degree_1[cur].vertex] = save[i];
		tmp[save[i]] = 1;
		if(take_answer(edge_1,edge_2,used,degree_1,degree_2,degree_1[cur].vertex,save[i],tmp,degree1,degree2,ret)){
			return 1;
		}
		for(int j = 0;j < _count;j++){
			used[ret[j]] = 0;
		}
		used[degree_1[cur].vertex] = 0;
		tmp[save[i]] = 0;
	}
}

void match(EDGE edge_1[],EDGE edge_2[],int n,int m,DEGREE degree_1[],DEGREE degree_2[],int degree1[],int degree2[]){
	int used[1100] = {0};
	int tmp[1100] = {0};
	for(int i = 1;i <= n;i++){ 
		if(used[degree_1[i].vertex] == 0){
			fix_first(edge_1,edge_2,degree_1,degree_2,used,n,i,tmp,degree1,degree2);
		}
	}
	for(int i = 1;i <= n;i++)
		cout << used[i] << " ";
	cout << "\n";
}

int main(){
	int T,n,m,p1,p2;
	cin >> T;
	for(int i = 0;i < T;i++){
		EDGE *edge_1 = new EDGE[1100];
		EDGE *edge_2 = new EDGE[1100];
		DEGREE *degree_1 = new DEGREE[1100];
		DEGREE *degree_2 = new DEGREE[1100];
		int ans[1100] = {0};
		int used[1100] = {0};
		int count = 0;
		
		cin >> n >> m;
		
		for(int j = 0;j <= n;j++){
			degree_1[j].vertex = j;
			degree_2[j].vertex = j;
			degree_1[j].degree = 0;
			degree_2[j].degree = 0;
			degree1[j] = 0;
			degree2[j] = 0;
		}
		
		degree_1[0].degree = 9999999;
		degree_2[0].degree = 9999999;
		
		for(int j = 0;j < m;j++){
			cin >> p1 >> p2;
			if(p1 == p2){
				edge_1[p1].selfloop++;
				continue;
			}
			edge_1[p1].edge[p2]++;
			edge_1[p2].edge[p1]++;
			if(edge_1[p1].edge[p2] >= 2){
				if(edge_1[p1].edge[p2] == 2)
					edge_1[p1].multi_point_count++;
				edge_1[p1].multi_edge[edge_1[p1].multi_edge_count] = p2;
				edge_1[p1].multi_edge_count++;
			}
			if(edge_1[p2].edge[p1] >= 2){
				if(edge_1[p2].edge[p1] == 2)
					edge_1[p2].multi_point_count++;
				edge_1[p2].multi_edge[edge_1[p2].multi_edge_count] = p1;
				edge_1[p2].multi_edge_count++;
			}
			edge_1[p1].point_connect[edge_1[p1].edge_connect] = p2;
			edge_1[p2].point_connect[edge_1[p2].edge_connect] = p1;
			edge_1[p1].edge_connect++;
			edge_1[p2].edge_connect++;
			degree_1[p1].degree++;
			degree_1[p2].degree++;
			degree1[p1]++;
			degree1[p2]++;
		}
		for(int j = 0;j < m;j++){
			cin >> p1 >> p2;
			if(p1 == p2){
				edge_2[p1].selfloop++;
				continue;
			}
			edge_2[p1].edge[p2]++;
			edge_2[p2].edge[p1]++;
			if(edge_2[p1].edge[p2] >= 2){
				//for more detail
				if(edge_2[p1].edge[p2] == 2)
					edge_2[p1].multi_point_count++;
				edge_2[p1].multi_edge[edge_2[p1].multi_edge_count] = p2;
				edge_2[p1].multi_edge_count++;
			}
			if(edge_2[p2].edge[p1] >= 2){
				//for more detail
				if(edge_2[p2].edge[p1] == 2)
					edge_2[p2].multi_point_count++;
				edge_2[p2].multi_edge[edge_2[p2].multi_edge_count] = p1;
				edge_2[p2].multi_edge_count++;
			}
			edge_2[p1].point_connect[edge_2[p1].edge_connect] = p2;
			edge_2[p2].point_connect[edge_2[p2].edge_connect] = p1;
			edge_2[p1].edge_connect++;
			edge_2[p2].edge_connect++;
			degree_2[p1].degree++;
			degree_2[p2].degree++;
			degree2[p1]++;
			degree2[p2]++;
		}
		sort(degree_1+1,degree_1+n+1,&waytosort);
		sort(degree_2+1,degree_2+n+1,&waytosort);
		/*for(int j = 1;j <= n;j++){
			sort(edge_1[j].point_connect,edge_1[j].point_connect+edge_1[j].edge_connect,sortedge);
			sort(edge_2[j].point_connect,edge_2[j].point_connect+edge_2[j].edge_connect,sortedge);
		}*/
		match(edge_1,edge_2,n,m,degree_1,degree_2,degree1,degree2);
	}
}