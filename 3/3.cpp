#include <iostream>

using namespace std;

long long fight(long long e,long long p,long long iaj){
	int bit[64] = {0};
	int i = 0;
	long long num = iaj;
	if(e == 0)
		return 1;
	while(e != 0){
		if(e % 2 != 0)
			bit[i] = 1;
		else
			bit[i] = 2;
		i++;
		e /= 2;
	}
	bit[i - 1] = 0;
	i -= 2;
	while(i >= 0){
		if(bit[i] == 2){
			num *= num;
			num %= p;
		}
		else if(bit[i] == 1){
			num *= num;
			num %= p;
			num *= iaj;
			num %= p;
		}
		i--;
	}
	return num;
}

void queue(long long winner[],long long c,long long e,long long p,long long left,long long right,long long suba[]){
	if(right == left + 1)
		return; 

	long long midpoint = (right - left)/2;	
	long long l = left,mid = left + midpoint;
	long long count = 0;
	long long temp = 0;
	long long imj = 0;
	
	queue(winner,c,e,p,left,left + midpoint,suba);
	queue(winner,c,e,p,left + midpoint,right,suba);
	
	for(int i = 0;i < right - left;i++){
		//count = fight(c,e,p,winner[l],winner[mid]);
		if(winner[l] - winner[mid] < 0){
			temp = (winner[l] - winner[mid]) * -1;
			imj = p - temp;
		}
		else
			imj = (winner[l] - winner[mid]) % p;
		long long exp = fight(e,p,(winner[l]+winner[mid]));
		if(l < left + midpoint && (mid == right || (((c * (imj%p) * (exp%p)) % p) > p/2))){
			suba[i] = winner[l];
			l++;
		}
		else{
			suba[i] = winner[mid];
			mid++;
		}
	}
	
	for(int i = left;i < right;i++){
		winner[i] = suba[i - left];
	}
	return;
}

int main(){
	long long T,n;
	long long c,e,p;
	cin >> T;
	//cin >> e >> p >> i >> j >> square;
	//cout <<  fight(e,p,i+j,square);
	//return 0;
	for(long long i = 0;i < T;i++){
		long long *winner = new long long[200000];
		long long *suba = new long long[200000];
		long long square = 0;
		
		cin >> n >> c >> e >> p;
		for(long long j = 0;j < n;j++){
			winner[j] = j + 1;
		}
		queue(winner,c,e,p,0,n,suba);	
		for(long long j = 0;j < n;j++)
			cout << winner[j] << " ";
		cout << "\n";
	}
	return 0;
}