#include <iostream>
#include <stdio.h>
#include <string.h>
#include <cstring>

using namespace std;

string new_string(int num){
	string s = "" ;
	int count = 0;
	while(num != 0){
		if(num >= 9){
			s = '9' + s;
			num -= 9;
		}
		else{
			s = (char)(num + 48) + s;
			num = 0;
		}
		//cout << count << " ";
		//count++;
	}
	return s;
}

void case_larger_0(int ci[1001],char si[1002][1300],int i,int num){
	string s = "";
	for(int j = strlen(si[i-1]) - 1;j >= 0;j--){
		//remember for the case si[i] = 1999999
		if(num == 0){
			s = si[i-1][j] + s;
			continue;
		}
		if(si[i-1][j] == '9'){
			s = '9' + s;
		}
		else{
			int temp = si[i-1][j] - '0';
			if(num < 9 - temp){
				s = (char)(num + temp + 48) + s;
				num = 0;
			}
			else{
				s = '9' + s;
				num -= (9 - temp);
			}
		}
	}
	if(num > 0)
		s = new_string(num) + s;
	
	strcpy(si[i],s.c_str());
	//cout << "larger 0 = " << si[i] << "\n";
}

void case_lower_0(int ci[1001],char si[1002][1300],char buf[1300],int i,int j){
	string s = "";
	int num = ci[i];
	int s_len = strlen(si[i-1]);
	//initialize s
	if(j == -1){
		s = '1';
		s_len += 1;
	}
	else{
		for(int k = 0;k < j;k++)
			s = s + buf[k];
		s = s + (char)(1 + buf[j] - '0' + 48);
	}
	
	for(int k = 0;k < s.length();k++)
		num -= s.at(k) - '0';
	
	string temp = new_string(num);
	int temp_len = s.length();
	for(int k = 0;k < s_len - temp.length() - temp_len;k++)
		s = s + '0';	
	s = s + temp;
	//cout << "s=" <<s;
	strcpy(si[i],s.c_str());
	//cout << "lower 0 = " << si[i] << "\n";
}

void ci_t_si(int ci[1001],char si[1002][1300],int i){
	int num = ci[i];
	
	//case for first num
	if(i == 0)
		strcpy(si[0],new_string(ci[0]).c_str());
	//cout << si[0];
	
	else{
		char buf[1300];
		int last_not_9 = -1;
		int len = strlen(si[i-1]);
		for(int j = 0;j < len;j++){
			num -= si[i - 1][j] - '0';
			//case: num <= 0
			if(num <= 0){
				case_lower_0(ci,si,buf,i,last_not_9);
				break;
			}
			else{
				buf[j] = si[i-1][j];
				//cout << buf[j];
			}
			
			if((si[i - 1][j] - '0') != 9 ){
				last_not_9 = j;
				//cout << "last 9=" << last_not_9 << "\n";
			}
		}
		
		//case: num > 0
		if(num > 0)
			case_larger_0(ci,si,i,num);
	}
}

void get_si(int ci[1001],int n){
	char si[1002][1300];
	for(int i = 0;i < n;i++){
		ci_t_si(ci,si,i);
	}
	for(int i = 0;i < n - 1;i++)
		cout << si[i] << " ";
	cout << si[n-1] << "\n";
}

int main(){
	int T,n;
	int ci[1001];
	cin >> T;
	for(int i = 0;i < T;i++){
		cin >> n;
		for(int j = 0;j < n;j++)
			cin >>ci[j];
		get_si(ci,n);
	}
}