#include <iostream>
#include <stdlib.h>

using namespace std;

typedef struct edge{
	int group;
	int u,v;
	int weight;
} EDGE;

int compare(const void *data1,const void *data2){
	EDGE *ptr1 = (EDGE *)data1;
	EDGE *ptr2 = (EDGE *)data2;
	if(ptr1->weight > ptr2->weight)
		return 1;
	else if(ptr1->weight < ptr2->weight)
		return -1;
	else
		return 0; 
}

int find_SET(int set[501],int node){
	return set[node];
}

void UNION(EDGE edge[500],int set[501],int u_id,int v_id,int n,int m){
	int min = (u_id < v_id)? u_id:v_id;
	int max = (u_id > v_id)? u_id:v_id;
	for(int i = 1;i <= n;i++){
		if(set[i] == max){
			set[i] = min;
		}
	}
	for(int i = 0;i < m;i++){
		if(edge[i].group == max)
			edge[i].group = min;
	}
}

void check_same_weight(EDGE edge[10000],int set2[501],int last_set2[501],int cur_index,int last,int n,int m,int& k){
	bool cycle = false;
	cout << "last = " << last << " cur_index = " << cur_index << "\n";
	for(int i = last;i <= cur_index;i++){
		for(int j = last;j <= cur_index;j++){
			if(i == j)
				continue;
			else{
				int u_id = find_SET(set2,edge[j].u);
				int v_id = find_SET(set2,edge[j].v);
				cout << "u = " << edge[j].u << " v = " << edge[j].v << "\n";
				cout << "u_id = " << u_id << " v_id = " << v_id << "\n";
				if(u_id != v_id){
					UNION(edge,set2,u_id,v_id,n,m);
				}
				else{
					cycle = true;
					break;
				}
			}
		}
		if(cycle == false){
			cout << "i = " << i << "\n";
			k++;
		}
		cycle = false;
		for(int j = 1;j <= n;j++){
			set2[j] = last_set2[j];
		}
	}
}

void find_MUST(EDGE edge[10000],int n,int m){
	int u,v,weight;
	int set[501] = {0};
	int set2[501] = {0};
	int last_set2[501] = {0};
	int total_weight = 0;
	int total_edge = 0;
	int smaller_weight = 0;
	int temp = 0;
	int last = 0;
	int k = 0;
	int last_k = 0;
	int cycled[501] = {0};

	//make set
	smaller_weight = edge[0].weight;
	for(int i = 1;i <= n;i++){
		set[i] = i;
		set2[i] = i;
		last_set2[i] = i;
	}
	for(int i = 0;i < m;i++)
		edge[i].group = -1;

	//
	for(int i = 0;i < m;i++){
		int u_id = find_SET(set,edge[i].u);
		int v_id = find_SET(set,edge[i].v);

		if(edge[i].weight > smaller_weight){
			cout << "change : ";
			for(int j = 1;j <= n;j++){
				set2[j] = set[j];
				last_set2[j] = set[j];
				cout << set2[j] << " ";
			}
			cout << "\n";
			last = i;
			smaller_weight = edge[i].weight;
		}
		if(u_id != v_id){		//no cycle
			cout << "u = " << edge[i].u << " v = " << edge[i].v << " ";
			cout << "u_id before " << find_SET(set,edge[i].u) << " v_id before " << find_SET(set,edge[i].v) << "\n";
			UNION(edge,set,u_id,v_id,n,m);
			cout << "change in union : ";
			for(int j = 1;j <= n;j++){
				cout << set[j] << " ";
			}
			cout << "\n";
			cout << "u_id after " << find_SET(set,edge[i].u) << " v_id after " << find_SET(set,edge[i].v) << "\n";
			total_weight += edge[i].weight;
			total_edge++;
		}
		else{		//cycle
			if(edge[i].weight == edge[i - 1].weight){		//equal && cycle == another MST
				if(cycled[u_id] != 0){
					total_weight += cycled[u_id] * edge[i].weight;
					total_edge += cycled[u_id];
				}
	//			else{
					int temp = 0;
					cout << "in\n";
					last_k = k;
					check_same_weight(edge,set2,last_set2,i,last,n,m,k);
					temp = k - 1 - last_k;
					total_weight -= temp * edge[i].weight;
					total_edge -= temp;
					cycled[u_id] = temp;
	//			}
			}
			else if(edge[i].weight < edge[i - 1].weight)
				cout << "sorting error\n";

		}
		if(edge[i].weight > smaller_weight){
			for(int j = 0;j < n;j++)
				cycled[j] = 0;
		}
	}
	cout << "weight = " << total_weight << " edge = " << total_edge << "\n";
}

int main(){
	int T,n,m,a,b,c;
	EDGE edge[10000];
	cin >> T;
	for(int i = 0;i < T;i++){
		cin >> n >> m;
		for(int j = 0;j < m;j++){
			cin >> a >> b >> c;
			edge[j].u = a;
			edge[j].v = b;
			edge[j].weight = c;
		}
		qsort(edge,m,sizeof(EDGE),compare);
		find_MUST(edge,n,m);
	}
}
